package activity3.src;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PassGenTest {
	PasswordGenerator a = new PasswordGenerator("abcdefghijklmnopqrstuvwxyz");
	PasswordGenerator b = new PasswordGenerator("AbCdeFgHiJkLmNoPqRsTuVwXyZ");
	PasswordGenerator c = new PasswordGenerator("IUFWEHIUDHQWIUDHQWIUDHQWIU");

	@Test
	public void weakTest() {
		System.out.println(a.generateWeakPassword());
		System.out.println(b.generateWeakPassword());
		System.out.println(c.generateWeakPassword());
	}
	
	@Test
	public void strongTest() {
		System.out.println(a.generateStrongPassword());
		System.out.println(b.generateStrongPassword());
		System.out.println(c.generateStrongPassword());
	}

}